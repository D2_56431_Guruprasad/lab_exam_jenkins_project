const express=require('express')
const app= express()
const cors=require('cors')
app.use(cors("*"))
app.use(express.json())
const routerEmp = require("./routes/emp");
app.use("/emp", routerEmp);

app.listen(4000,"0.0.0.0",()=>{
    console.log("server started on port number 4000")
});